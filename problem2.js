// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
//"Last car is a *car make goes here* *car model goes here*"

function makeAndModel(inventory) {
  let obj = inventory[inventory.length - 1];

  if (obj) {
    return `Last car is a ${obj["car_make"]} ${obj["car_model"]}`;
  } else {
    return "No element";
  }
}
module.exports = makeAndModel;
