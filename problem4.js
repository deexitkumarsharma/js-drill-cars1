// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

function years(inventory) {
  let list = [];
  if (inventory.length == 0) {
    return list;
  }
  for (let index = 0; index < inventory.length; index++) {
    let obj = inventory[index];
    list.push(obj["car_year"]);
  }
  return list;
}
module.exports = years;
