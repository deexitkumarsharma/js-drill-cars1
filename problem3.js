// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

function sortAlphabetically(inventory) {
  let carModelSorted = [];
  for (let index = 0; index < inventory.length; index++) {
    carModelSorted.push(inventory[index].car_model);
  }

  return carModelSorted.sort();
}

module.exports = sortAlphabetically;
