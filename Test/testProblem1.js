let car_inventory = require("../inventory");
let carInformation = require("../problem1");
let carData = carInformation(car_inventory, 33);
console.log(
  "Car 33 is a",
  carData["car_year"],
  carData["car_make"],
  carData["car_model"]
);
